require_relative '../providers/profile_provider.rb'

class ProfileController < ApplicationController
  def index
    @presenter = ProfileProvider.profile_presenter_for_user current_user.id, nil
    render
  end

  def show
    id = params['id']
    @presenter = ProfileProvider.profile_presenter_for_user current_user.id, id
    render
  end
end
