=begin

# Profile-related
name :get_profiles_for_user
input :user_id
return {:profile_id => :profile_title}

name :create_profile_for_user
input :user_id, :profile_title
return :profile_id

name :delete_profile
input :user_id, :profile_id
return :success

name :update_profile_name
input :user_id, :profile_id, :new_name
return :success

name :update_profile_objective
input :user_id, :profile_id, :new_objective
return :success

name :get_contact_info
input :user_id
return {:address, :phone, :email, :web}

name :get_subsection_info
input :user_id, :subsection_id
return {:content, :tags [{:title, :type, :tag_id}]}

name :get_tag_info
input :tag_id, :user_id
return {/* todo later */}

name :get_career_objective_id
input :user_id, :profile_id
return {:subsection_id}

name :get_cv_subjections
input :user_id
return {[:subsection_id]}
=end

class ServiceBroker
  def get_data(service_call, input_data)
    case service_call
      when :get_profiles_for_user
        a = {:id => '1', :title => 'Web Developer'}
        b = {:id => '2', :title => 'Team Lead'}
        return {:profiles => [a,b]}
      when :update_profile_name
        return {:success => true}
    end
  end
end

