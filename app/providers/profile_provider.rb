

module ProfileProvider
  def ProfileProvider.profile_presenter_for_user(user_id, active_profile_id)
    presenter = {}
    presenter[:profile_nav] = ProfileProvider.profile_nav_for_user user_id, active_profile_id
    return presenter
  end

  def ProfileProvider.profile_nav_for_user(user_id, active_profile_id)
    nav = {}

    data = ServiceBroker.new.get_data :get_profiles_for_user, {:user_id => user_id}
    nav[:profiles] = data[:profiles]
    if !active_profile_id.nil?
      nav[:profiles].each do |profile|
        if active_profile_id == profile[:id]
          nav[:active_profile] = profile
        end
      end
    end
    return nav
  end
end